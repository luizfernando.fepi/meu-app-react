import React from "react";
import { Modal, Form, Button, Table, ButtonGroup } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

class Alunos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            nome: '',
            email: '',
            modalAberta: false,
            mostraTabela: true,
            alunos: []
        }

        this.deletarAluno = this.deletarAluno.bind(this);
        this.buscaAlunos = this.buscaAlunos.bind(this);
        this.cadastraAluno = this.cadastraAluno.bind(this);
        this.atualizaNome = this.atualizaNome.bind(this);
        this.atualizaEmail = this.atualizaEmail.bind(this);
        this.submit = this.submit.bind(this);
        this.fecharModal = this.fecharModal.bind(this);
        this.abrirModal = this.abrirModal.bind(this);
    }

    componentDidMount() {
        this.buscaAlunos();
    }

    buscaAlunos() {
        fetch("https://localhost:5001/api/alunos")
            .then(resposta => resposta.json())
            .then(dados => {
                this.setState({ alunos: dados })
            })
    }

    cadastraAluno(aluno) {
        fetch("https://localhost:5001/api/alunos", {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(aluno)
        }).then(resposta => {
            if (resposta.ok) {
                this.buscaAlunos();
            } else {
                alert('Não foi possível adicionar o aluno!');
            }
        })
    }

    carregarDados(id) {
        fetch("https://localhost:5001/api/alunos/" + id)
            .then(resposta => resposta.json())
            .then(aluno => {
                this.setState(
                    {
                        id: aluno.id,
                        nome: aluno.nome,
                        email: aluno.email
                    }
                )
                this.abrirModal()
            })
    }

    atualizarAluno(aluno) {
        fetch("https://localhost:5001/api/alunos/" + aluno.id, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(aluno)
        }).then(resposta => {
            if (resposta.ok) {
                this.buscaAlunos();
            } else {
                alert("Erro ao atualizar!");
            }
        })
    }

    deletarAluno(id) {
        fetch("https://localhost:5001/api/alunos/" + id, { method: 'DELETE' })
            .then((resposta) => {
                if (resposta.ok) {
                    this.buscaAlunos();
                }
            })
    }


    renderTabela() {

        const listaAlunos = this.state.alunos.map((aluno) =>
            <tr key={aluno.id}>
                <td>{aluno.nome}</td>
                <td>{aluno.email}</td>
                <td>
                    <Button icon="" class="bttable" variant="outline-primary" onClick={() => this.carregarDados(aluno.id)}>Atualizar</Button>
                    <Button class="bttable" variant="outline-dark" onClick={() => this.deletarAluno(aluno.id)}>Deletar</Button>
                </td>
            </tr>
        )


        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tbody>
                    {listaAlunos}
                </tbody>
            </Table>
        )

    }

    fecharModal() {
        this.setState({
            modalAberta: false,
            id: 0,
            nome: '',
            email: ''
        })
    }

    abrirModal() {
        this.setState({
            modalAberta: true
        })
    }

    renderForm() {
        return (
            <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Preencha os dados</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form id="cadastro">
                        <div class="mb-3">
                            <label class="form-label">Nome</label>
                            <input type="text" class="form-control" value={this.state.nome} onChange={this.atualizaNome}></input>
                            <br />
                            <label class="form-label">Email</label>
                            <input type="email" class="form-control" value={this.state.email} onChange={this.atualizaEmail}></input>
                            <br />
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.fecharModal}>Cancelar</Button>
                    <Button form="cadastro" variant="primary" onClick={this.submit}>Confimar 2</Button>
                </Modal.Footer>
            </Modal>
        )
    }

    atualizaNome(e) {
        this.setState(
            {
                nome: e.target.value
            }
        )
    }

    atualizaEmail(e) {
        this.setState(
            {
                email: e.target.value
            }
        )
    }

    submit() {
        const aluno = {
            id: this.state.id,
            nome: this.state.nome,
            email: this.state.email
        }
        if (this.state.id == 0) {
            this.cadastraAluno(aluno);
        } else {
            this.atualizarAluno(aluno);
        }
        this.fecharModal();
    }

    mostraTabela = () => {
        this.setState({
            mostraTabela: true
        })
    }

    mostraLista = () => {
        this.setState(
            { mostraTabela: false }
        )
    }

    render() {
        return (
            <div class="tabelaAlunos">
                <Button class="bt" variant="primary" onClick={this.abrirModal}>Adicionar Aluno</Button>
                <br />
                <br />
                {this.renderForm()}
                {this.renderTabela()}
            </div>
        )
    }
}


export default Alunos;