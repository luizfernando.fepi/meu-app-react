import React from 'react';

export default function Sobre() {
   return (
      <div>
         <br />
         <p> Está aplicação foi desenvolvida com <strong>React</strong>.</p>
         <p> Um CRUD de alunos foi implementado. Este front-end consome uma Web API. As seguintes tecnologias foram utilizadas:</p>
         <ul>
            <li>ASP.NET Core</li>
            <li>Entity Framework Core</li>
            <li>PostgreSQL</li>
            <li>ReactJS</li>
         </ul>
      </div>)
}